function createCalendar(id, year, month) {
	let elem = document.getElementById(id);
	elem.innerHTML = '';
	let mon = month - 1; // месяцы в JS идут от 0 до 11, а не от 1 до 12
	let d = new Date(year, mon);

	let table = '<table id="table_calendar"><tr><th>пн</th><th>вт</th><th>ср</th><th>чт</th><th>пт</th><th>сб</th><th>вс</th></tr><tr>';

	for (let i = 0; i < getDay(d); i++) {
		table += '<td class="inactive-calendar"></td>';
	}
	while (d.getMonth() === mon) {
		table += '<td class="day'+ d.getDate() +'" onclick="openDayNote('+ d.getDate() +');">' + d.getDate() + '</td>';

		if (getDay(d) % 7 === 6) { // вс, последний день - перевод строки
			table += '</tr><tr>';
		}

		d.setDate(d.getDate() + 1);
	}

	// добить таблицу пустыми ячейками, если нужно
	if (getDay(d) !== 0) {
		for (let i = getDay(d); i < 7; i++) {
			table += '<td class="inactive-calendar"></td>';
		}
	}

	// закрыть таблицу
	table += '</tr></table>';

	// только одно присваивание innerHTML
	elem.innerHTML = table;
}

function getDay(date) { // получить номер дня недели, от 0(пн) до 6(вс)
	let day = date.getDay();
	if (day === 0) {
		day = 7;
	}
	return day - 1;
}

document.querySelector(".month").onchange = () => {
	createCalendar("calendar", document.querySelector(".year").value, document.querySelector(".month").value);
	for(const evs of document.querySelectorAll(".is_event")){
		evs.classList.remove('is_event');
	}
	ipcRenderer.send("loadNotes");
};

document.querySelector(".year").onchange = () => {
	createCalendar("calendar", document.querySelector(".year").value, document.querySelector(".month").value);for(const evs of document.querySelectorAll(".is_event")){
		evs.classList.remove('is_event');
	}
	ipcRenderer.send("loadNotes");
};
