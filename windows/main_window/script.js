const electron = require('electron');
const { ipcRenderer } = electron;

let _notes = [];

let time_stamp = null;

document.getElementById("notes").onclick = () => {
	if(!document.getElementById("notes").classList.contains("active")){
		document.querySelector(".active").classList.remove("active");
		document.getElementById("notes").classList.add("active");
		document.querySelector(".notes-content").style.display = 'block';
		document.querySelector(".calendar").style.display = 'none';
		document.querySelector(".addNote").style.display = 'block';
	}
	document.querySelector(".notes-view").style.top = "-100%";
	if(document.querySelector(".delete").classList.contains("inactive")){
		document.querySelector(".delete").classList.remove("inactive");
	}
	document.querySelector(".note-text").classList.remove(document.querySelector(".note-text").classList[1]);
	document.querySelector(".notes-view").classList.remove(document.querySelector(".notes-view").classList[1]);
};

document.getElementById("diary").onclick = () => {
	if(!document.getElementById("diary").classList.contains("active")){
		document.querySelector(".active").classList.remove("active");
		document.getElementById("diary").classList.add("active");
		document.querySelector(".notes-content").style.display = 'none';
		document.querySelector(".calendar").style.display = 'block';
		document.querySelector(".addNote").style.display = 'none';
	}
	document.querySelector(".notes-view").style.top = "-100%";
	if(document.querySelector(".delete").classList.contains("inactive")){
		document.querySelector(".delete").classList.remove("inactive");
	}
	document.querySelector(".note-text").classList.remove(document.querySelector(".note-text").classList[1]);
	document.querySelector(".notes-view").classList.remove(document.querySelector(".notes-view").classList[1]);
};

document.querySelector(".exit").onclick = () => {
	document.querySelector(".notes-view").style.top = "-100%";
	if(document.querySelector(".delete").classList.contains("inactive")){
		document.querySelector(".delete").classList.remove("inactive");
	}
	document.querySelector(".note-text").classList.remove(document.querySelector(".note-text").classList[1]);
	document.querySelector(".notes-view").classList.remove(document.querySelector(".notes-view").classList[1]);
};

document.querySelector(".addNote").onclick = () => {
	openNote(null);
};

function KeyPress(e) {
	let evtobj = window.event ? event : e;
	if (evtobj.keyCode === "N".charCodeAt(0) && evtobj.ctrlKey){
		openNote(null);
	}
	if (evtobj.keyCode === 27){
		document.querySelector(".notes-view").style.top = "-100%";
		if(document.querySelector(".delete").classList.contains("inactive")){
			document.querySelector(".delete").classList.remove("inactive");
		}
		document.querySelector(".note-text").classList.remove(document.querySelector(".note-text").classList[1]);
	}
}

document.onkeydown = KeyPress;

window.onload = () => {
	ipcRenderer.send("loadNotes");
	let date = new Date();
	let month = date.getMonth() + 1;
	let year = date.getFullYear();
	createCalendar("calendar", year, month);
	document.querySelector(".year").value = year;
	document.querySelector(".month").value = month;
};

ipcRenderer.on('allNotes', (e, notes) => {
	document.querySelector(".notes-content").innerHTML = '';
	_notes = [];
	let i = 1;
	let month = document.querySelector(".month").value;
	let year = document.querySelector(".year").value;
	if(null !== notes){
		for(const note of notes){
			note['id'] = i;
			if(null === note.time_stamp){
				addViewNote(note);
			}
			else {
				const time_date = note.time_stamp.split(".");
				if(parseInt(time_date[1]) === parseInt(month) && parseInt(time_date[2]) === parseInt(year)){
					document.querySelector('.day' + parseInt(time_date[0])).classList.add("is_event");
				}
			}
			i++;
			_notes.push(note);
		}
	}
});

function addViewNote(note){
	if(note.hasOwnProperty("text") && note.hasOwnProperty("date") && note.hasOwnProperty("id")){
		let divNote = document.createElement('div');
		divNote.className = `notes-item ${note.id} underline`;
		divNote.setAttribute("onclick", `openNote(${note.id})`);
		let divText = document.createElement('div');
		divText.className = "name-notes";
		if(note.text.indexOf("<") >= 0){
			divText.innerHTML = note.text.substring(0, note.text.indexOf("<"));
		}
		else {
			divText.innerHTML = note.text.substring(0, 25);
			if(note.text.length > 25){
				divText.innerHTML += '...';
			}
		}
		let divDate = document.createElement('div');
		divDate.className = "create-time";
		divDate.innerHTML = note.date;

		divNote.appendChild(divText);
		divNote.appendChild(divDate);
		document.querySelector(".notes-content").appendChild(divNote);
	}
	else {
		console.log("Error data!");
	}
}

function openNote(id){
	if(null === id){
		document.querySelector(".note-text").innerHTML = '';
		document.querySelector(".delete").classList.add("inactive");
	}
	else {
		document.querySelector(".note-text").classList.add(id);
		document.querySelector(".note-text").innerHTML = _notes[id - 1].text;
	}
	document.querySelector(".notes-view").style.top = 0;
}

document.querySelector(".b-btn").onclick = () => {
	document.execCommand('bold', false);
	document.querySelector(".note-text").focus();
};

document.querySelector(".i-btn").onclick = () => {
	document.execCommand('italic', false);
	document.querySelector(".note-text").focus();
};

document.querySelector(".u-btn").onclick = () => {
	document.execCommand('underline', false);
	document.querySelector(".note-text").focus();
};

document.querySelector(".list-btn").onclick = () => {
	document.execCommand('insertunorderedlist', false);
	document.querySelector(".note-text").focus();
};

document.querySelector(".save").onclick = () => {
	if(void 0 !== document.querySelector(".notes-view").classList[1]){
		time_stamp = document.querySelector(".notes-view").classList[1];
	}
	let id = document.querySelector(".note-text").classList[1];
	if(void 0 === id){
		let date = new Date();
		let notes_id = _notes.length + 1;
		let note = {
			text: document.querySelector(".note-text").innerHTML,
			id: notes_id,
			date: `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`,
			time_stamp: time_stamp
		};

		document.querySelector(".note-text").classList.add(notes_id);
		_notes.push(note);
	}
	else {
		_notes[id - 1].text = document.querySelector(".note-text").innerHTML;
	}
	document.querySelector(".notes-view").classList.remove(time_stamp);
	time_stamp = null;
	document.querySelector(".delete").classList.remove("inactive");
	ipcRenderer.send("syncNotes", _notes);
	ipcRenderer.send("loadNotes");
};

document.querySelector(".delete").onclick = () => {
	if(!document.querySelector(".delete").classList.contains("inactive")){
		const ts = _notes[document.querySelector(".note-text").classList[1] - 1].time_stamp;
		if(null !== ts){
			document.querySelector('.day' + parseInt(ts.split(".")[0])).classList.remove("is_event");
		}
		_notes.splice(document.querySelector(".note-text").classList[1] - 1, 1);
	}
	ipcRenderer.send("syncNotes", _notes);
	ipcRenderer.send("loadNotes");
	document.querySelector(".notes-view").style.top = "-100%";
	document.querySelector(".note-text").classList.remove(document.querySelector(".note-text").classList[1]);
	document.querySelector(".notes-view").classList.remove(document.querySelector(".notes-view").classList[1]);
};

function openDayNote(_day){
	const year = document.querySelector(".year").value;
	const month = document.querySelector(".month").value;
	const day = _day;
	const date = `${day}.${month}.${year}`;
	const id = _notes.find(function(obj){
		if(obj.time_stamp === date){
			return obj;
		}
		else {
			return void 0;
		}
	});
	if(void 0 === id){
		openNote(null);
		document.querySelector(".notes-view").classList.add(date);
	}
	else {
		openNote(id.id);
	}
}
