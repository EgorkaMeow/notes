const fs = require('fs');
const electron = require('electron');
const url = require('url');
const path = require('path');

const { app, BrowserWindow, ipcMain } = electron;

let mainWindow;

let notes = [];

app.on('ready', () => {
	mainWindow = new BrowserWindow({
		resizable: false,
	    width: 800,
	    height: 600,
	    autoHideMenuBar: true
	});

	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'windows/main_window/mainWindow.html'),
		protocol: 'file:',
		slashes: true
	}));

	readDataFromFile();
});

ipcMain.on('loadNotes', () => {
	mainWindow.webContents.send('allNotes', notes);
});

ipcMain.on('syncNotes', (e, _notes) => {
	notes = _notes;
	saveNotes(_notes);
});

function readDataFromFile(){
	fs.readFile('data/data.json', (err, data) => {
		console.log(data);
		if(err){
			if('ENOENT' !== err.code){
				throw err;
			}
		}
		if(void 0 !== data){
			notes = JSON.parse(data);
		}
	});
}

function saveNotes(notes){
	fs.writeFile('data/data.json', JSON.stringify(notes), (err) => {
		if (err){
			throw err;
		}
	});
}
